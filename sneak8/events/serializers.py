from rest_framework import serializers

from .models import Event,Comment
from drf_haystack.serializers import HaystackSerializer
from search_indexes import EventIndex



class EventSerializer(serializers.ModelSerializer):

	class Meta:
		model = Event
		fields = ('id','business','title','event_date','startTime','endTime','description','views','price','photo')



class CommentSerializer(serializers.ModelSerializer):

	class Meta:
		model = Comment
		fields = ('id','user','content','event','created','visibility')