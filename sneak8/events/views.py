from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework import filters 
from rest_framework import generics
from rest_framework.response import Response 
from rest_framework import status
from rest_framework.decorators import api_view
import django_filters
from .serializers import EventSerializer,CommentSerializer
from .models import Event,Comment
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from sneak8.users.models import User 
from django.http import JsonResponse
from django.core import serializers
from .search_indexes import EventIndex 
from django.db.models import Q
import simplejson as json 
# Create your views here.



#Api views

class EventCreateReadView(ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    lookup_field = 'slug'



class EventReadUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    lookup_field = 'slug'


#Filtering for the Event object for the RESTful API call
class EventFilter(django_filters.FilterSet):
    # TODO  Figure out how to filter for dates less than, greater than, and equal to a date
    title = django_filters.CharFilter(lookup_type="icontains")          #sets lookup type to allow api to filter for text 'containing' the searched value.otherwise it'd only retreive EXACT matches.
    description = django_filters.CharFilter(lookup_type="icontains")
    #event_date = django_filters.DateFilter(lookup_type=('gte') )
    class Meta:
        model = Event
        fields = [
        'id',
        'business', 
        'title', 
        'event_date', 
        'startTime',
        'endTime',
        'description',
        'views']

        order_by = ['event_date']


class EventList(generics.ListAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EventFilter


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)



@api_view(['GET','POST'])
def event_list(request):
    """
    List all Events, or create a new event"
    """

    print " in event list"

    if request.method == 'POST':
        print "in server post.testing"
        print "Request body: " + request.body
        json_data = request.read()      #json_data contains the data uploaded in request
        data = json.loads(json_data)
        print "title data: %s" % data['title']

        """
        for data in request.data:
            print "Request data: %s" % data  
        mylist = request.data
        if mylist['title'] is not None:
            titleList = mylist['title'].split()
            print titleList
        """
        print "title data: %s" % data['title']
        titleList = data['title'].split()
        events = Event.objects.filter(reduce(lambda x, y: x | y, [Q(title__icontains=word) for word in titleList]))
        serializer = EventSerializer(events,many=True)
        
        return Response(serializer.data,status=status.HTTP_201_CREATED)



@csrf_exempt
def event_detail(request, pk):
    """
    Retrieve, update or delete a code event.
    """
    try:
        event = Event.objects.get(pk=pk)
    except Event.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = EventSerializer(event)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = EventSerializer(event, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        event.delete()
        return HttpResponse(status=204)


# API views for Comment object

#Api views
class CommentCreateReadView(ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    lookup_field = 'slug'



class CommentReadUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    lookup_field = 'slug'


#Filtering for the Event object for the RESTful API call
class CommentFilter(django_filters.FilterSet):
    # TODO  Figure out how to filter for dates less than, greater than, and equal to a date
   
    class Meta:
        model = Comment
        fields = [
        'id',
        'user', 
        'content', 
        'event', 
        'created',
        'visibility']

        order_by = ['created']


class CommentList(generics.ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CommentFilter



@csrf_exempt    # Makes this function exempt from having a csrf token
def mobile_create_event(request):



    if request.POST:
        title = request.POST['title']
        locationname = request.POST['locationname']
        startTime = request.POST['startTime']
        endTime = request.POST['endTime']
        addressline1 = request.POST['addressline1']
        addressline2 = request.POST['addressline2']
        city = request.POST['city']
        state = request.POST['state']
        zipcode = request.POST['zipcode']
        description = request.POST['description']
        category = request.POST['category']
        price = request.POST['price']
        photo_base64 = request.POST['photo']  #if program failing, come back to this..Make sure client sends using base64 encoding
        username = request.POST['username']
        event_date =request.POST['event_date']      #Gets the date for the event
    
    return HttpResponse("OK. Event Created");



@csrf_exempt    # Makes this function exempt from having a csrf token
def mobile_create_comment(request):



    if request.POST:
        print "Post items: "

        for key, value in request.POST.iteritems():
            print "key:" + key + " - value: " +value


        user_id = request.POST.get('user_id')
        content = request.POST['content']
        event_id = request.POST['event_id']
        #created = request.POST['created_date']
        #visibility = request.POST['visibility']

        user = User.objects.get(id=user_id)
        event = Event.objects.get(id=event_id)

        try:
            comment = Comment(user=user,event=event,content=content)
            comment.save()      # Creates the new comment

            return HttpResponse("OK. Comment created")
        except:
            return HttpResponse(status=500)
    



@csrf_exempt    # Makes this function exempt from having a csrf token
def mobile_getSuggestedEvents(request):


        # Print out POST data for testing purposes
    if request.POST:
        print "Post items: "

        for key, value in request.POST.iteritems():
            print "key:" + key + " - value: " +value


    suggestedEvents = Event.objects.all()


    
class suggestedEvents(generics.ListAPIView):
    serializer_class = EventSerializer

    # TODO add in 'request' argument as second parameter
    def get_queryset(self):
        """
        This view should return a list of events suggested for the specified user
        """
        print self.request.method  # testing to see what method printed 

        # add this if statement later-->   if self.request == 'POST' :    #do something

        businessId = 1
        return Event.objects.filter(business_id=businessId)