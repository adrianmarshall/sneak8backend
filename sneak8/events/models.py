from django.db import models
from django.db import models
from django.utils import timezone
from sneak8.businesses.models import Business 
import datetime 
import os


# For comment visiblity 

CONTENT_VISIBLE = '0'
CONTENT_HIDDEN = '1'
CONTENT_DELETED = '2'
CONTENT_VISIBILITY = [
    [CONTENT_DELETED, 'Deleted'],
    [CONTENT_HIDDEN, 'Hidden'],
    [CONTENT_VISIBLE, 'Visible']
]

	#function that creates and returns a dynamic file path to save the image at. i.e- 'photos/events/22/myImage.png'
def get_image_path(instance,filename):
	return os.path.join('photos/events',str(instance.id),filename)


class Event(models.Model):

	business = models.ForeignKey(Business) 			## Foreign key to a business
	likes = models.ManyToManyField("users.User", related_name='likes')
	title = models.CharField(max_length=75)
	event_date = models.DateField(blank=True,default=datetime.date.today)		# changed to Date field.. migrate database
	startTime = models.DateTimeField(blank=True,default=datetime.datetime.now) 
	endTime = models.DateTimeField(blank=True,null=True)
	description = models.TextField(blank=True)
	price = models.PositiveIntegerField(default=0)
	views = models.ManyToManyField("users.User", related_name='views')
	photo = models.ImageField(upload_to=get_image_path,blank=True,null=True)
	preferednight = models.CharField(max_length=75)
	preferednightType = models.CharField(max_length=75)


	def __unicode__(self):
		return self.title 

	class Meta:
		ordering=['startTime']



class Comment(models.Model):
    user = models.ForeignKey("users.User", related_name='comments')
    content = models.TextField()						# Enforce a character limit at the app level. Not enforcing here in case we need to change.
    event = models.ForeignKey(Event, related_name='comments')			# This would allow me to do something with an Event Object myevent like e.g: myevent.comments.all() , to get all the comments for that event
    created = models.DateTimeField(auto_now_add=True)
    visibility = models.CharField(max_length=1, choices=CONTENT_VISIBILITY, default=CONTENT_VISIBLE)
