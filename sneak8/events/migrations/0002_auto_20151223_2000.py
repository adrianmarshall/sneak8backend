# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='event_date',
            field=models.DateField(default=datetime.date.today, blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='startTime',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
    ]
