# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import sneak8.events.models


class Migration(migrations.Migration):

    dependencies = [
        ('businesses', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=75)),
                ('event_date', models.DateField(default=datetime.date.today, null=True)),
                ('startTime', models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)),
                ('endTime', models.DateTimeField(null=True, blank=True)),
                ('description', models.TextField(blank=True)),
                ('views', models.PositiveIntegerField(default=0, null=True)),
                ('price', models.PositiveIntegerField(default=0)),
                ('photo', models.ImageField(null=True, upload_to=sneak8.events.models.get_image_path, blank=True)),
                ('business', models.ForeignKey(to='businesses.Business')),
            ],
            options={
                'ordering': ['startTime'],
            },
        ),
    ]
