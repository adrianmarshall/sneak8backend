# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_auto_20160110_0207'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='preferednight',
            field=models.CharField(default=datetime.datetime(2016, 1, 11, 1, 0, 48, 129228, tzinfo=utc), max_length=75),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='preferednightType',
            field=models.CharField(default=datetime.datetime(2016, 1, 11, 1, 1, 5, 713043, tzinfo=utc), max_length=75),
            preserve_default=False,
        ),
    ]
