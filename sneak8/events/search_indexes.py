#
# search_indexes.py
#

from django.utils import timezone
from haystack import indexes
from .models import Event



class EventIndex(indexes.SearchIndex,indexes.Indexable):

	text = indexes.CharField(document=True,use_template=True)
	title = indexes.CharField(model_attr="title")
	description = indexes.CharField(model_attr="description")



	#@staticmethod
	#def prepare_autocomplete(obj):
	#	return " ".join( (
	#		obj.title,obj.description
	#		))

	def get_model(self):
		return Event

	def index_queryset(self,using=None):
		return self.get_model().objects.all()