# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url,include,patterns
from django.views.decorators.csrf import csrf_exempt

from . import views

#TODO come back ,remove csrf exempt and find a way to implement 
# csrf tokens for security reasons to prevent hijacking

urlpatterns = [
    # api url for User to Create, Read, View 
url(
    regex=r'^~api/v1/$',
    view = csrf_exempt(views.EventCreateReadView.as_view() ),
    name="event_rest_api"
    ),
# api url for sneak search, send POST data in JSON format for 'title',search through titles
url(
    regex=r'^~api/v1/sneaksearch/$',
    view = views.event_list,
    name="event_search_api"
    ),


    #api url for User to Update and Delete
url(
    regex=r'^~api/v1/update/(?P<slug>[-\w]+)/$',
    view = views.EventReadUpdateDeleteView.as_view(),
    name="event_rest_api"
    ),

     #api to filter/search for Events. Mainly filters by event attirbutes. does not search through events
    url(
        regex=r'^~api/v1/search/$',
        view = csrf_exempt(views.EventList.as_view()),
        name="event_rest_api"
        ),

     # API URL's for comments for events
     url(
    regex=r'^comments/~api/v1/$',
    view = csrf_exempt(views.CommentCreateReadView.as_view()),
    name="comment_rest_api"
    ),

         #api url for User to Update and Delete comments
url(
    regex=r'^comments/~api/v1/update/(?P<slug>[-\w]+)/$',
    view = views.CommentReadUpdateDeleteView.as_view(),
    name="comment_rest_api"
    ),

     #api to filter/search for comments
    url(
        regex=r'^comments/~api/v1/search/$',
        view = views.CommentList.as_view(),
        name="comment_rest_api"
        ),
    #api to create for comments
    url(
        regex=r'^comments/~api/v1/create/$',
        view = views.mobile_create_comment,
        name="comment_rest_api"
        ),
    #api to get list of suggested events
    url(
        regex=r'^getsuggested/~api/v1/$',
        view = views.suggestedEvents.as_view(),
        name="getsuggested_api"
        ),


]