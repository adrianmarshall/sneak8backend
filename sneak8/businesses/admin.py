from django.contrib import admin

# Register your models here.
from .models import Business

class BusinessAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'state')



admin.site.register(Business, BusinessAdmin)