from rest_framework import serializers

from .models import Business

class BusinessSerializer(serializers.ModelSerializer):

	class Meta:
		model = Business
		fields = ('id','owner','name','Addressline1','Addressline2','city','state','zipcode','phone_number')