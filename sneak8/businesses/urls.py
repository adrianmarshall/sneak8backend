# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # api url for User to Create, Read, View 
url(
    regex=r'^~api/v1/$',
    view = views.BusinessCreateReadView.as_view(),
    name="business_rest_api"
    ),

    #api url for User to Update and Delete
url(
    regex=r'^~api/v1/update/(?P<slug>[-\w]+)/$',
    view = views.BusinessReadUpdateDeleteView.as_view(),
    name="business_rest_api"
    ),

     #api to filter/search for Businesses
    url(
        regex=r'^~api/v1/search/$',
        view = views.BusinessList.as_view(),
        name="business_rest_api"
        ),

]