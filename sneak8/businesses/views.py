from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework import filters 
from rest_framework import generics
import django_filters

from .serializers import BusinessSerializer
from .models import Business
# Create your views here.



#Api views

class BusinessCreateReadView(ListCreateAPIView):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    lookup_field = 'slug'



class BusinessReadUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    lookup_field = 'slug'

#Filtering for the Business object for the RESTful API call
class BusinessFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_type="icontains")          #sets lookup type to allow api to filter for text 'containing' the searched value.otherwise it'd only retreive EXACT matches.

    class Meta:
        model = Business
        fields = ['id','owner', 'name', 'Addressline1', 'Addressline2','city','state','zipcode']

class BusinessList(generics.ListAPIView):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = BusinessFilter