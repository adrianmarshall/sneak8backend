# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.db import models
from django.core.validators import RegexValidator
#from sneak8.users.models import User   	#Commented out and used text to import User model to get around circular dependency

class Business(models.Model):

	owner = models.ForeignKey("users.User",on_delete=models.CASCADE)
	name = models.CharField(blank=False,max_length=200)					#Name for the business
	Addressline1 = models.CharField(blank=False,max_length=200)						#address fields
	Addressline2 = models.CharField(blank=True,max_length=200)						# are here
	city = models.CharField(blank=False,max_length=200)
	state = models.CharField(blank=False,max_length=200)
	zipcode = models.CharField(blank=False,max_length=200)
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

	phone_number = models.CharField(max_length=15,validators=[phone_regex], blank=True) # validators should be a list



	def __str__(self):
		return self.name
