from rest_framework import serializers

from .models import User



class UserSerializer(serializers.ModelSerializer):
	password = serializers.CharField(write_only=True)

	class Meta:
		model = User
		fields = ('id','username','email','password','first_name','last_name','is_staff','type')
		read_only_fields = ['is_staff']
        write_only_fields = ['password']

     # Update the Users fields, call set_password function to Hash the password
	def update(self,instance,validated_data):
		for attr, value in validated_data.items():
			if attr == 'password':
				instance.set_password(value)
			else:
				setattr(instance,attr,value)

		instance.save()
		return instance

	# Creates new User object and uses set_password to Hash the users password
	def create(self,validated_data):
		password = validated_data.pop('password',None)
		instance = self.Meta.model(**validated_data)
		if password is not None:
			instance.set_password(password)
		instance.save()
		return instance

		