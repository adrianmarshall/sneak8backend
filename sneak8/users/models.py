# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from sneak8.events.models import Event   #TODO Fix cirulcar dependency
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _



TYPES = (('regular','regular'),('business','business'))			#List of different types of users we can have. Regular users and Business users

	#function that creates and returns a dynamic file path to save the image at. i.e- 'photos/22/myImage.png'
def get_image_path(instance,filename):
	return os.path.join('photos/user',str(instance.id),filename)

@python_2_unicode_compatible
class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of User"), blank=True, max_length=255)
    type = models.CharField(max_length=10,choices=TYPES,default='regular')
    profile_image = models.ImageField(upload_to=get_image_path,blank=True,null=True)
    favorites = models.ManyToManyField("events.Event", related_name='favorited_by')        #TODO Fix circular dependency, cannot import Event object


    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})
