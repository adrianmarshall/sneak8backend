# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sneak8.users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='profile_image',
            field=models.ImageField(null=True, upload_to=sneak8.users.models.get_image_path, blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='type',
            field=models.CharField(default='regular', max_length=10, choices=[('regular', 'regular'), ('business', 'business')]),
        ),
    ]
